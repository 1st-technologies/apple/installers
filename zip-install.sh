#!/bin/bash


## User Variables
url="$4"
arm64URL="$5"
__version__="0.1.3"


# Global Variables
zipFile=$(mktemp)
tmpDir=$(mktemp -d)


# Get Architecture
if [[ "$HOSTTYPE" != "x86_64" ]]; then
	if [[ -n $arm64URL ]]; then
		url=$arm64URL
	fi
fi


echo "Downloading ${url}..."
curl -sSL --retry-all-errors --retry 3 "$url" -o "$zipFile" || exit $?


echo "Unpacking ZIP file to temporary directory..."
unzip -q "$zipFile" -d "$tmpDir" || exit $?


echo "Setting current directory to ${tmpDir}..."
pushd "${tmpDir}" > /dev/null || exit $?


echo "Searching for applications in ZIP..."
appName=$(find . -maxdepth 1 -type d -iname "*.app" | head -n 1)


if [[ -z $appName ]]; then
	echo "Error: No .app found in the ZIP image, aborting."
	exit 1
fi


appName=$(basename "$appName")
procName=$(basename "$appName" .app) 


echo "Kill $procName if running..."
pkill "$procName" || exit $?


echo "Checking if /Applications/${appName} already exists..." 
if [[ -d "/Applications/$appName" ]]; then
	echo "/Applications/${appName} found, deleting..." 
	rm -R "/Applications/$appName" || exit $?
fi


echo "Copying ${appName} to /Applications..."
ditto -rsrc "./$appName" "/Applications/$appName" || exit $?


echo "Leaving ${tmpDir}..."
popd > /dev/null || exit $?


echo "Setting ownership on /Applications/${appName}..."
chown -R root:wheel "/Applications/$appName" || exit $?


echo "Setting permissions on /Applications/${appName}..."
chmod 755 "/Applications/$appName" || exit $?


echo "Removing quarantine extended attribute on /Applications/${appName}..."
xattr -d com.apple.quarantine "/Applications/${appName}" 2> /dev/null  || exit $?


echo "Deleting ${zipFile}..."
rm "$zipFile" || exit $?


echo "Deleting ${tmpDir}..."
rm -R "$tmpDir" || exit $?


exit 0