# zip-pkg-install

Download and install a PKG in a ZIP.

```mermaid
flowchart TB
    start([Start])
    download[[Download ZIP]]
    unpack[Unpack ZIP]
    scanZip[[Scan<br />ZIP for PKG]]
    ifPkgFound{PKG Found?}
    scanPkg[[Scan PKG<br />for Apps]]
    delTemp[Delete<br />Unpacked Files]
    delZip[Delete ZIP]
    ifAppFound{App Found?}
    ifRunning{App Running?}
    kill[Kill Process]
    ifInstalled{Already<br />Installed?}
    install[Install PKG]
    delApp[Delete App]
    ending([End])

    start --> download
    download --> unpack
    unpack --> scanZip
    scanZip --> ifPkgFound
    ifPkgFound -- No --> delTemp
    ifPkgFound -- Yes --> scanPkg
    delTemp --> delZip
    delZip --> ending
    scanPkg --> ifAppFound
    ifAppFound -- Yes --> ifRunning
    ifAppFound -- No --> delTemp
    ifRunning -- Yes --> kill
    ifRunning -- No --> ifInstalled
    kill --> ifInstalled
    ifInstalled -- Yes --> delApp
    ifInstalled -- No --> install
    delApp --> install
    install --> delTemp
```

## Exit Codes

* **1** - No PKG found in ZIP
* **2** - No APP found in PKG
