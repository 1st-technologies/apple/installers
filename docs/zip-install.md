# zip-install

Download and install an APP in a ZIP.

```mermaid
flowchart TB
    start([Start])
    download[[Download ZIP]]
    unpack[Unpack ZIP]
    scan[[Scan<br />ZIP for App]]
    ifFound{App Found?}
    ifRunning{App Running?}
    kill[Kill Process]
    ifInstalled{Already<br />Installed?}
    deleteApp[Delete App]
    copy[Copy App to<br />Applications Folder]
    setOwn[[Set App Ownership,<br /> Permissions and<br />remove Quarantine]]
    unmount[Delete Unpacked Files]
    delZip[Delete ZIP]
    ending([End])

    start --> download
    download --> unpack
    unpack --> scan
    scan --> ifFound
    ifFound -- No --> delZip
    ifFound -- Yes --> ifRunning
    ifRunning -- Yes --> kill
    ifRunning -- No --> ifInstalled
    kill --> ifInstalled
    ifInstalled -- Yes --> deleteApp
    ifInstalled -- No --> copy
    deleteApp --> copy
    copy --> setOwn
    setOwn --> unmount
    unmount --> delZip
    delZip --> ending
```

## Exit Codes

* **1** - No APP found in ZIP
