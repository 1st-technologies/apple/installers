# zip-pkg-install

Download and install a PKG in a ZIP for Jamf.

```mermaid
flowchart TB
    start([Start])
    download[[Download DMG]]
    mount[Mount DMG]
    scanDMG[[Scan<br />DMG for PKG]]
    ifPkgFound{PKG Found?}
    scanPkg[[Scan PKG<br />for Apps]]
    deleteDmg[Delete DMG]
    unmount[Unmount DMG]
    deleteDmg[Delete DMG]
    ifAppFound{App Found?}
    ifRunning{App Running?}
    kill[Kill Process]
    ifInstalled{Already<br />Installed?}
    install[Install PKG]
    delApp[Delete App]
    ending([End])

    start --> download
    download --> mount
    mount --> scanDMG
    scanDMG --> ifPkgFound
    ifPkgFound -- Yes --> scanPkg
    ifPkgFound -- No --> unmount
    kill --> ifInstalled
    unmount --> deleteDmg
    deleteDmg --> ending
    scanPkg --> ifAppFound
    ifAppFound -- Yes --> ifRunning
    ifAppFound -- No --> unmount
    ifRunning -- Yes --> kill
    ifRunning -- No --> ifInstalled
    ifInstalled -- Yes --> delApp
    delApp --> install
    ifInstalled -- No --> install
    install --> unmount
```

## Exit Codes

* **1** - No PKG found in DMG
* **2** - No APP found in PKG
