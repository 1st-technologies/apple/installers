# pkg-install

Download and install an APP in a PKG.

```mermaid
flowchart TB
    start([Start])
    download[[Download PKG]]
    scan[[Scan PKG<br />for Apps]]
    ifFound{App Found?}
    ifRunning{App Running?}
    kill[Kill Process]
    ifInstalled{Already<br />Installed?}
    deleteApp[Delete App]
    install[Install PKG]
    deletePkg[Delete PKG]
    ending([End])

    start --> download
    download --> scan
    scan --> ifFound
    ifFound -- Yes --> ifRunning
    ifFound -- No --> deletePkg
    ifRunning -- Yes --> kill
    ifRunning -- No --> ifInstalled
    kill --> ifInstalled
    ifInstalled -- Yes --> deleteApp
    ifInstalled -- No --> install
    deleteApp --> install
    install --> deletePkg
    deletePkg --> ending
```

## Exit Codes

* **1** - No APP found in PKG
