# dmg-install

Download and install an APP in a DMG.

```mermaid
flowchart TB
    start([Start])
    download[[Download DMG]]
    mount[Mount DMG]
    scan[[Scan Mounted<br />DMG for App]]
    ifFound{App Found?}
    ifRunning{App Running?}
    kill[Kill Process]
    ifInstalled{Already<br />Installed?}
    deleteApp[Delete App]
    copy[Copy App to<br />Applications]
    setOwn[[Set App Ownership,<br />Permissions and<br />remove Quarantine]]
    unmount[Unmount DMG]
    deleteDmg[Delete DMG]
    ending([End])

    start --> download
    download --> mount
    mount --> scan
    scan --> ifFound
    ifFound -- Yes --> ifRunning
    ifFound -- No --> unmount
    ifRunning -- Yes --> kill
    ifRunning -- No --> ifInstalled
    kill --> ifInstalled
    ifInstalled -- Yes --> deleteApp
    ifInstalled -- No --> copy
    deleteApp --> copy
    copy --> setOwn
    setOwn --> unmount
    unmount --> deleteDmg
    deleteDmg --> ending
```

## Exit Codes

* **1** - No APP found in DMG
