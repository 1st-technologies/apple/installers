#!/bin/bash


## User Variables
url="$4"
arm64URL="$5"
__version__="0.1.3"


# Global Variables
zipFile=$(mktemp)
tmpDir=$(mktemp -d)


# Get Architecture
if [[ "$HOSTTYPE" != "x86_64" ]]; then
	if [[ -z $arm64URL ]]; then
		url=$arm64URL
	fi
fi


echo "Downloading ${url}..."
curl -sSL --retry-all-errors --retry 3 "$url" -o "$zipFile" || exit $?


echo "Unpacking ZIP file to temporary directory..."
unzip "$zipFile" -d "$tmpDir"


echo "Setting current directory to ${tmpDir}..."
pushd "${tmpDir}" > /dev/null || exit $?


echo "Searching for PKG files in ZIP"
pkgFile="$(find . -maxdepth 1 -type f -iname "*.pkg" | head -n 1)"

if [[ -z "$pkgFile" ]]; then
	echo "Error: No PKG files found in ZIP, exiting."
	exit 1
fi

echo "Getting Application name..."
appName=$(pkgutil --payload-files "${pkgFile}" | grep -i ".app" | cut -d "/" -f 2 | sort | uniq | head -n 1)
appName=$(basename "$appName")
procName=$(basename "$appName" .app) 


echo "Checking if PKG contains apps..."
if [[ -n $appName ]]; then
	echo "Found ${appName}, killing ${procName} if running..."
	pkill "$procName"
	echo "Checking if /Applications/${appName} already exists..." 
	if [[ -d "/Applications/$appName" ]]; then
		echo "/Applications/${appName} found, deleting..." 
		rm -R "/Applications/$appName" || exit $?
	fi
else
	echo "No apps found in pkg..."
	exit 2
fi


echo "Installing ${procName}..."
installer -pkg "${pkgFile}" -target / || exit $?


echo "Deleting ${zipFile}..."
rm "$zipFile" || exit $?


echo "Deleting ${tmpDir}..."
rm -R "$tmpDir" || exit $?


exit 0