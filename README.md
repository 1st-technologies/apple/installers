# Jamf Installers

A suite of lightweight installers for Jamf.

To download and install...

* a PKG file, use [pkg-install](docs/pkg-install.md)
* an APP in a DMG file, use [dmg-install](docs/dmg-install.md)
* an APP in a ZIP file ,use [zip-install](docs/zip-install.md)
* a PKG in a ZIP file, use [zip-pkg-install](docs/zip-pkg-install.md)
* a PKG in a DMG file, use [dmg-pkg-install](docs/dmg-pkg-install.md)

## Usage

Add to the Jamf scripts catalog and create a script policy with the following parameters:

**x86_64 or universal url**: The x86_64 (Intel) or Universal download URL.

**arm64 url**: The arm64 (Apple Silicon) download URL.

**NOTE**: In Jamf, parameters 1 through 3 are predefined as mount point, computer name, and username.
