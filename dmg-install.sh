#!/bin/bash


## User Variables
url="$4"
arm64URL="$5"
__version__="0.1.2"


# Global Variables
dmgFile=$(mktemp)
mountPoint=/Volumes/dmgtmp


# Get Architecture
if [[ "$HOSTTYPE" != "x86_64" ]]; then
	if [[ -n $arm64URL ]]; then
		url=$arm64URL
	fi
fi


echo "Downloading ${url}..."
curl -sSL --retry-all-errors --retry 3 "$url" -o "$dmgFile" || exit $?


echo "Mounting ${dmgFile}..."
map=$(hdiutil mount "${dmgFile}" -nobrowse -mountpoint $mountPoint) || exit $?


echo "Getting mouted DMG device ID..."
devindex=$(echo "$map" | grep dmgtmp | cut -f 1 -d  ' ')


echo "Setting current directory to ${mountPoint}..."
pushd "${mountPoint}" > /dev/null || exit $?


echo "Searching for applications in DMG..."
appName=$(find . -maxdepth 1 -type d -iname "*.app" | head -n 1)


if [[ -z $appName ]]; then
	echo "Error: No .app found in the DMG image, unmounting image..."
	hdiutil unmount "${mountPoint}" -quiet || exit $?
	echo "Ejecting image..."
	hdiutil eject "$devindex" -quiet || exit $?
	echo "Deleting image..."
    rm "$dmgFile" || exit $?
	exit 1
fi


appName=$(basename "$appName")
procName=$(basename "$appName" .app) 


echo "Kill $procName if running..."
pkill "$procName"


echo "Checking if /Applications/${appName} already exists..." 
if [[ -d "/Applications/$appName" ]]; then
	echo "/Applications/${appName} found, deleting..." 
	rm -R "/Applications/$appName" || exit $?
fi


echo "Copying ${appName} to /Applications..."
ditto -rsrc "./$appName" "/Applications/$appName" || exit $?


echo "Leaving ${mountPoint}..."
popd > /dev/null || exit $?


echo "Setting ownership on /Applications/${appName}..."
chown -R root:wheel "/Applications/$appName" || exit $?


echo "Setting permissions on /Applications/${appName}..."
chmod 755 "/Applications/$appName" || exit $?


echo "Removing quarantine extended attribute on /Applications/${appName}..."
xattr -d com.apple.quarantine "/Applications/${appName}" 2> /dev/null || exit $?


echo "Unmounting ${mountPoint}..."
hdiutil unmount "${mountPoint}" -quiet || exit $?


echo "Ejecting ${devindex}..."
hdiutil eject "$devindex" -quiet || exit $?


echo "Deleting ${dmgFile}..."
rm "$dmgFile" || exit $?


exit 0