#!/bin/bash


## User Variables
url="$4"
arm64URL="$5"
__version__="0.1.3"


# Global Variables
pkgFile=$(mktemp)


# Get Architecture
if [[ "$HOSTTYPE" != "x86_64" ]]; then
	if [[ -n $arm64URL ]]; then
		url=$arm64URL
	fi
fi


echo "Downloading ${url}..."
curl -sSL --retry-all-errors --retry 3 "$url" -o "$pkgFile" || exit $?
echo "Renaming file..."
mv "$pkgFile" "${pkgFile}.pkg" || exit $?


echo "Getting Application name..."
appName=$(pkgutil --payload-files "${pkgFile}.pkg" | grep -i ".app" | cut -d "/" -f 2 | sort | uniq | head -n 1)
appName=$(basename "$appName")
procName=$(basename "$appName" .app) 


echo "Checking if PKG contains apps..."
if [[ -n $appName ]]; then
	echo "Found ${appName}, killing ${procName} if running..."
	pkill "$procName" || exit $?
	echo "Checking if /Applications/${appName} already exists..." 
	if [[ -d "/Applications/$appName" ]]; then
		echo "/Applications/${appName} found, deleting..." 
		rm -R "/Applications/$appName" || exit $?
	fi
else
	echo "No apps found in pkg..."
	exit 1
fi


echo "Installing ${procName}..."
installer -pkg "${pkgFile}.pkg" -target / || exit $?


echo "Deleting PKG..."
rm "${pkgFile}.pkg" || exit $?


exit 0