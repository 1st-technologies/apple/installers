#!/bin/bash


## User Variables
url="$4"
arm64URL="$5"
__version__="0.1.3"


# Global Variables
dmgFile=$(mktemp)
mountPoint=/Volumes/dmgtmp


# Get Architecture
if [[ "$HOSTTYPE" != "x86_64" ]]; then
	if [[ -n $arm64URL ]]; then
		url=$arm64URL
	fi
fi


echo "Downloading ${url}..."
curl -sSL --retry-all-errors --retry 3 "$url" -o "$dmgFile" || exit $?


echo "Mounting ${dmgFile}..."
map=$(hdiutil mount "${dmgFile}" -nobrowse -mountpoint $mountPoint) || exit $?


echo "Getting mouted DMG device ID..."
devindex=$(echo "$map" | grep dmgtmp | cut -f 1 -d  ' ')


echo "Setting current directory to ${mountPoint}..."
pushd "${mountPoint}" > /dev/null || exit $?


echo "Searching for PKG in DMG..."
pkgFile=$(find . -maxdepth 1 -type d -iname "*.pkg" | head -n 1)


if [[ -z $pkgFile ]]; then
	echo "Error: No .pkg found in the DMG image, unmounting image..."
	hdiutil unmount "${mountPoint}" -quiet || exit $?
	echo "Ejecting image..."
	hdiutil eject "$devindex" -quiet || exit $?
	echo "Deleting image..."
    rm "$dmgFile" || exit $?
	exit 1
fi

echo "Getting Application name..."
appName=$(pkgutil --payload-files "${pkgFile}" | grep -i ".app" | cut -d "/" -f 2 | sort | uniq | head -n 1)
appName=$(basename "$appName")
procName=$(basename "$appName" .app) 


echo "Checking if PKG contains apps..."
if [[ -n $appName ]]; then
	echo "Found ${appName}, killing ${procName} if running..."
	pkill "$procName"
	echo "Checking if /Applications/${appName} already exists..." 
	if [[ -d "/Applications/$appName" ]]; then
		echo "/Applications/${appName} found, deleting..." 
		rm -R "/Applications/$appName" || exit $?
	fi
else
	echo "No app found in pkg..."
	exit 2
fi


echo "Installing ${procName}..."
installer -pkg "${pkgFile}" -target / || exit $?


echo "Leaving ${mountPoint}..."
popd > /dev/null || exit $?


echo "Unmounting ${mountPoint}..."
hdiutil unmount "${mountPoint}" -quiet || exit $?


echo "Ejecting ${devindex}..."
hdiutil eject "$devindex" -quiet || exit $?


echo "Deleting ${dmgFile}..."
rm "$dmgFile" || exit $?


exit 0